package com.example.app.model;

import lombok.*;


@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class FileRow {

    private String key;
    private String value;

    public String toString() {
        return this.getKey() + " " + this.getValue();
    }
}
