package com.example.app.model;

import org.json.simple.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class JSONObjectBuilder extends JSONObject {
    private Map<String, JSONObject> objectValPair;
    private Map<String, String> stringValPair;

    public JSONObjectBuilder() {
        this.objectValPair = new HashMap<>();
        this.stringValPair = new HashMap<>();
    }

    public JSONObject build() {
        JSONObject result = new JSONObject();
        result.putAll(this.objectValPair);

        result.putAll(this.stringValPair);
        return result;
    }

    public JSONObjectBuilder pairWithObjectVal(String key, JSONObject json) {
        this.objectValPair.put(key, json);
        return this;
    }

    public JSONObjectBuilder pairWithStringVal(String key, String value) {
        this.stringValPair.put(key, value);
        return this;
    }
}
