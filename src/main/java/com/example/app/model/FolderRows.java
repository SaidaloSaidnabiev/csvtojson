package com.example.app.model;

import lombok.*;
import org.json.simple.JSONObject;

import java.util.List;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class FolderRows {
    private String folderName;
    private List<JSONObject> children;
}
