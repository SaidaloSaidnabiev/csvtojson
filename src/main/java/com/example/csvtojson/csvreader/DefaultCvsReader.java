package com.example.csvtojson.csvreader;

import com.example.app.model.FileRow;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
class DefaultCvsReader implements CvsReader {

    private static final char CSV_VALUE_SPLITTER = ';';
    private static final int KEY_INDEX = 0;
    private static final int VALUE_INDEX = 1;
    private static final String ENCODING = "windows-1251";
    private static final CSVParser parser = new CSVParserBuilder().withSeparator(CSV_VALUE_SPLITTER).build();

    @Override
    public List<FileRow> readFrom(String fileName) {
        Path myPath = Paths.get(fileName);
        try (var buffer = Files.newBufferedReader(myPath, Charset.forName(ENCODING))) {
            var reader = new CSVReaderBuilder(buffer).withCSVParser(parser).build();
            return reader.readAll()
                    .stream()
                    .filter(matchesTwoElements())
                    .map(toFileRow())
                    .collect(Collectors.toList());
        } catch (IOException | CsvException e) {
            throw new CsvFileNotFound(fileName, e);
        }
    }

    @Override
    public List<FileRow> readFromFolder(String folderPath) {
        Optional<File[]> opt = Optional.ofNullable(new File(folderPath).listFiles());
        return opt.map(listOfFiles -> Arrays.stream(listOfFiles)
                .map(this::mapFileContent)
                .flatMap(List::stream)
                .collect(Collectors.toList())).orElse(Collections.emptyList());
    }

    private Predicate<String[]> matchesTwoElements() {
        return array -> array.length == 2;
    }

    private Function<String[], FileRow> toFileRow() {
        return joined -> new FileRow(joined[KEY_INDEX], joined[VALUE_INDEX]);
    }

    private List<FileRow> mapFileContent(File file) {
        return readFrom(file.getPath())
                .stream().peek(fileRow -> new FileRow(file.getName() + ":" + fileRow.getKey(), fileRow.getValue()))
                .collect(Collectors.toList());
    }

}
