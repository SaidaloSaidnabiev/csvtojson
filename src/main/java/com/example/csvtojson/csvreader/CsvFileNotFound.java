package com.example.csvtojson.csvreader;

class CsvFileNotFound extends RuntimeException {

    CsvFileNotFound(String fileName, Exception exception) {
        super(String.format("Error reading file: %s!", fileName), exception);
    }

}
