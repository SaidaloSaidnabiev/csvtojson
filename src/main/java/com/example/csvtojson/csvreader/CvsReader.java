package com.example.csvtojson.csvreader;

import com.example.app.model.FileRow;

import java.util.List;

/**
 * Service that can read CSV file and return collection of {@link FileRow}
 */
public interface CvsReader {

    /**
     * Searches for file in provided path
     * <p>
     * In case when file is not found or there is a reading error, then {@link CsvFileNotFound}
     *
     * @param fileName
     * @return
     */
    List<FileRow> readFrom(String fileName);

    List<FileRow> readFromFolder(String folderPath);

}
