package com.example.csvtojson.csvparser;

import com.example.app.model.FileRow;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * Interface for CSV file parsing class
 */
public interface CsvParser {
    /**
     * Accepts list of FileRow model from CSV file
     * and parses into Json object.
     * Returns that parsed object.
     * @param fileContentList
     * @return
     */
    JSONObject parseListToJson(List<FileRow> fileContentList);
}
