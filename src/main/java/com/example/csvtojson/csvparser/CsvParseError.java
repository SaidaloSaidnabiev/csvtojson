package com.example.csvtojson.csvparser;

class CsvParseError extends RuntimeException{
    CsvParseError(Exception e) {
        super("Error while parsing! Please check CSV structure or try again", e);
    }
}
