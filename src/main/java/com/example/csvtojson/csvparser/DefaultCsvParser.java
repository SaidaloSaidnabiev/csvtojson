package com.example.csvtojson.csvparser;

import com.example.app.model.FileRow;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
class DefaultCsvParser implements CsvParser {
    private static final String CSV_KEY_SPLITTER = ":";

    @Override
    public JSONObject parseListToJson(List<FileRow> fileContentList) {
        JSONObject rootObject = new JSONObject();
        fileContentList.forEach(
            fileRow -> putFilerowInObject(rootObject, fileRow));
        return rootObject;
    }

    private void putFilerowInObject(JSONObject rootObject, FileRow fileRow) {
        String[] keys = fileRow.getKey().split(CSV_KEY_SPLITTER);
        JSONObject tempObj = iterateOver(rootObject, Arrays.copyOfRange(keys, 0, keys.length-1));
        tempObj.put(keys[keys.length-1], fileRow.getValue());
    }

    private JSONObject iterateOver(JSONObject rootObject, String[] keys) {
        if(keys.length > 0) {
            rootObject = (JSONObject) rootObject.computeIfAbsent(keys[0], k -> new JSONObject());
            return iterateOver(rootObject, Arrays.copyOfRange(keys, 1, keys.length));
        }
        return rootObject;
    }
}
