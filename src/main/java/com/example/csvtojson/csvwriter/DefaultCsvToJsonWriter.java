package com.example.csvtojson.csvwriter;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class DefaultCsvToJsonWriter implements CsvToJsonWriter {

  @Override
  public File writeToJsonFile(File myFile, JSONObject jsonObject) {
      try (FileWriter file = new FileWriter(myFile)) {
          file.write(jsonObject.toJSONString());
          file.flush();
      } catch (Exception e) {
          e.printStackTrace();
      }
    return myFile;
  }

  @Override
  public List<File> writeToJsonFiles(String destination, JSONObject parsedFile) {
      Set<Map.Entry<String, JSONObject>> set = parsedFile.entrySet();
      return set.stream()
            .map(
                entry -> writeToJsonFile(
                        new File(createFoldersForPath(destination, entry.getKey())), entry.getValue()))
            .collect(Collectors.toList());
  }

    private String createFoldersForPath(String saveDestination, String key) {
        String wholePath = saveDestination + key;
        String basePath = wholePath.substring(0, wholePath.lastIndexOf(File.separator));
        File file = new File(basePath);
        file.mkdirs();
        return wholePath;
    }
}
