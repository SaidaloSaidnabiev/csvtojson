package com.example.csvtojson.csvwriter;

import com.example.app.model.FileRow;
import org.json.simple.JSONObject;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Interface of Writer interface
 */
public interface CsvToJsonWriter {
    /**
     * Accepts Destination file and json object for dest. file content.
     * Returns back modified file.
     * @param myFile
     * @param myObject
     * @return
     */
    File writeToJsonFile(File myFile, JSONObject myObject);

    /**
     * Accepts destination directory and Json object as parameters
     * @param destination
     * @return list of generated files
     */
    List<File> writeToJsonFiles(String destination, JSONObject parsedFile);
}
