package com.example;

import com.example.app.model.FileRow;
import com.example.app.model.FolderRows;
import com.example.csvtojson.csvparser.CsvParser;
import com.example.csvtojson.csvreader.CvsReader;
import com.example.csvtojson.csvwriter.CsvToJsonWriter;
import com.example.jsontocsv.jsonparser.JsonParser;
import com.example.jsontocsv.jsonreader.JsonReader;
import com.example.jsontocsv.jsontocsv.ToCsvWriter;
import org.json.simple.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@SpringBootApplication
public class SpringBootParseTranslations implements CommandLineRunner {
    private final JsonReader jsonReader;
    private final JsonParser jsonParser;
    private final ToCsvWriter toCsvWriter;
    private final CsvParser csvParser;
    private final CvsReader csvReader;
    private final CsvToJsonWriter toJsonWriter;
    private static final String SLASH = File.separator;
    private final String appResource = Paths.get(System.getProperty("user.dir"),"src", "main", "resources") + SLASH;
    private static final String TEST_RESOURCES = Paths.get(System.getProperty("user.dir"),"src", "test", "resources") + SLASH;
    private final String sourcePath =  Paths.get(appResource, "json") + SLASH;
    private final String destinationPath = Paths.get(appResource, "csv") + SLASH;
    private final String localePath = Paths.get(appResource, "jsonFiles") + SLASH;
    private static final String DESTINATION_PATH_PATH = TEST_RESOURCES + "json" + SLASH;
    private static final String CSV_TO_MULTIPLE_JSON = "1";
    private static final String FOLDER_CONTENT_TO_BIG_JSON = "2";
    private static final String MULTI_LANG_INTERACTION = "3";

    public SpringBootParseTranslations(JsonReader jsonReader,
                                       JsonParser jsonParser,
                                       ToCsvWriter toCsvWriter,
                                       CsvParser csvParser,
                                       CvsReader csvReader,
                                       CsvToJsonWriter toJsonWriter) {
        this.jsonReader = jsonReader;
        this.jsonParser = jsonParser;
        this.toCsvWriter = toCsvWriter;
        this.csvParser = csvParser;
        this.csvReader = csvReader;
        this.toJsonWriter = toJsonWriter;
    }

    public static void main(String[] args) throws IOException{
        System.out.println("STARTING THE APPLICATION");
        boolean repeat = true;
        ConfigurableApplicationContext context;
        BufferedReader consoleReader = new BufferedReader(
                new InputStreamReader(System.in));

        String option;
        while(repeat) {
            context = SpringApplication.run(SpringBootParseTranslations.class, args);
            System.out.print("Try again Y/N:");
            option = consoleReader.readLine();
            if (Objects.equals(option.toLowerCase(Locale.ROOT), "n")) {
                repeat = false;
            }
            context.close();
        }
        System.out.println("APPLICATION FINISHED");
    }

    @Override
    public void run(String... args) throws IOException {
        System.out.println("Choose action:");
        System.out.println("1-Csv to Multiple Json\t2-Folder content to big CSV\t3-Multiple language interaction");
        System.out.print("Your choice:");

        BufferedReader consoleReader = new BufferedReader(
                new InputStreamReader(System.in));

        String option = consoleReader.readLine();
        int tries = 0;
        while(!inOptionList(option)) {
            if (tries > 5) {
                System.exit(0);
            }
            System.out.println("Invalid choice, please choose proper option!!");
            System.out.print("Your choice:");
            option = consoleReader.readLine();
            tries++;
        }
        switch (option) {
            case CSV_TO_MULTIPLE_JSON:
                csvToMultipleJson();
                break;
            case FOLDER_CONTENT_TO_BIG_JSON:
                currentApplication();
                break;
            case MULTI_LANG_INTERACTION:
                multiLangInteraction();
                break;
            default:
                break;
        }
    }

    private void multiLangInteraction() {
        List<JSONObject> jsons = jsonReader.readJsonFolder(appResource + SLASH + "lt");
        List<FileRow> parsedJsons = jsonParser.parseFolderContentIntoList(jsons);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        File generatedFile = toCsvWriter.writeToCsv(destinationPath + timestamp.getTime() + "_translation.csv", parsedJsons);
        System.out.println("FILE:" + generatedFile.getPath());
        System.out.println("Successfully GENERATED!");
    }

    private void csvToMultipleJson() {
        List<FileRow> bigFileContent = csvReader.readFrom(destinationPath + "1642544119696_translation.csv");
        JSONObject parsedFile = csvParser.parseListToJson(bigFileContent);
        List<File> generatedFiles = toJsonWriter.writeToJsonFiles(DESTINATION_PATH_PATH, parsedFile);
        printGeneratedFiles(generatedFiles);
    }

    private void currentApplication() {
        List<FolderRows> folderRows = jsonReader.readJsonsForLanguages(sourcePath);
        List<FileRow> wholeList = jsonParser.parseIntoBigList(folderRows);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        toCsvWriter.writeToCsv(destinationPath + timestamp.getTime() + "_translation.csv", wholeList);

        // Parsing backwards
        List<FileRow> bigFileContent = csvReader.readFrom(destinationPath + "1642544119696_translation.csv");
        JSONObject parsedFile = csvParser.parseListToJson(bigFileContent);
        toJsonWriter.writeToJsonFiles(localePath, parsedFile);
    }

    private void printGeneratedFiles(List<File> files) {
        System.out.println("Files:");
        files.forEach(file -> System.out.println(file.getName()));
        System.out.println("GENERATED!!");
    }

    private static boolean inOptionList(String finalOption) {
    return Objects.equals(finalOption, CSV_TO_MULTIPLE_JSON)
        || Objects.equals(finalOption, FOLDER_CONTENT_TO_BIG_JSON)
        || Objects.equals(finalOption, MULTI_LANG_INTERACTION);
    }
}
