package com.example.jsontocsv.jsontocsv;

import com.example.app.model.FileRow;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Interface for CSV writing
 */
public interface ToCsvWriter {
    /**
     * Accepts destination location for .csv file and write content collection
     * Returns csv file path.
     * @param destFilePath
     * @param toBeWritten
     * @return
     */
    File writeToCsv(String destFilePath, List<FileRow> toBeWritten);

    /**
     * Accepts location where to save files and object of parsed jsons as parameters
     * @param saveDestination
     * @param parsedJsons
     * @return Map of grouped file row objects by filename
     */
    List<File> writeIntoCsvFiles(String saveDestination, List<FileRow> parsedJsons);
}
