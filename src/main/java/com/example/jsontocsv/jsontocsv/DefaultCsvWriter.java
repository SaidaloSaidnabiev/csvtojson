package com.example.jsontocsv.jsontocsv;

import com.example.app.model.FileRow;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.opencsv.CSVWriter;

@Service
public class DefaultCsvWriter implements ToCsvWriter {

  @Override
  public File writeToCsv(String destFilePath, List<FileRow> toBeWritten) {
    File file = new File(destFilePath);
    try (CSVWriter writer =
        new CSVWriter(
            new FileWriter(file),
            ';',
            CSVWriter.NO_QUOTE_CHARACTER,
            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
            CSVWriter.RFC4180_LINE_END)) {
      toBeWritten.forEach(
          fileRow -> {
            String[] writableRow = {fileRow.getKey(), fileRow.getValue()};
            writer.writeNext(writableRow);
          });

    } catch (IOException e) {
      e.printStackTrace();
    }
    return file;
  }

  @Override
  public List<File> writeIntoCsvFiles(String saveDestination, List<FileRow> parsedJsons) {
    // Grouping by file name on fileRow key
    return parsedJsons.stream()
        .collect(
            Collectors.groupingBy(
                this::getFilename, Collectors.mapping(this::myMapper, Collectors.toList())))
        .entrySet()
        .stream()
        .map(
            mySet ->
                writeToCsv(
                    saveDestination + "/" + mySet.getKey().replace(".json", ".csv"),
                    mySet.getValue()))
        .collect(Collectors.toList());
  }

  private FileRow myMapper(FileRow fileRow) {
    String fileName = getFilename(fileRow);
    return new FileRow(fileRow.getKey().replace(fileName + ":", ""), fileRow.getValue());
  }

  private String getFilename(FileRow fileRow) {
    return fileRow.getKey().split(":")[0];
  }
}
