package com.example.jsontocsv.jsonparser;

import com.example.app.model.FileRow;
import com.example.app.model.FolderRows;
import org.json.simple.JSONObject;

import java.util.List;

/** Interface class for Parser and its declaration
 *
 */
public interface JsonParser {
    /**
     * Accepts json from file and parses into list of objects
     * @param myObject
     * @return
     */
    List<FileRow> parseIntoList(JSONObject myObject);

    /**
     * Accepts list of file's contents in form of List
     * @param fileContents
     * @return parsed list of keys and its values of whole folder files in singe list
     */
    List<FileRow> parseFolderContentIntoList(List<JSONObject> fileContents);

    /**
     * Accepts list of folder contents as parameter
     * @param folderRows
     * @return list of FileRow which has filepath on it's key
     */
    List<FileRow> parseIntoBigList(List<FolderRows> folderRows);
}
