package com.example.jsontocsv.jsonparser;

import com.example.app.model.FileRow;
import com.example.app.model.FolderRows;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
class DefaultJsonParser implements JsonParser {
    @Override
    public List<FileRow> parseIntoList(JSONObject myObject) {
        return this.innerParse("", myObject);
    }

    @Override
    public List<FileRow> parseFolderContentIntoList(List<JSONObject> fileContents) {
        return fileContents.stream()
                .map(this::parseIntoList)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public List<FileRow> parseIntoBigList(List<FolderRows> folderRows) {
        return folderRows.stream().map(this::parseFolderRow).flatMap(List::stream).collect(Collectors.toList());
    }

    private List<FileRow> innerParse(String objectKey, JSONObject myObject) {
        Set<String> myKeys = myObject.keySet();
        return myKeys.stream()
                .map(key -> createFileRow(key, objectKey, myObject))
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<FileRow> createFileRow(String key, String objectKey, JSONObject myObject) {
        String updatedKey = objectKey.isEmpty() ? key : objectKey + ":" + key;
        return Optional.of(myObject.get(key))
                .filter(String.class::isInstance)
                .map(f -> Arrays.asList(new FileRow(updatedKey, (String) f)))
                .orElseGet(() -> this.innerParse(updatedKey, (JSONObject) myObject.get(key)));
    }

    private List<FileRow> parseFolderRow(FolderRows folderRow) {
    return this.parseFolderContentIntoList(folderRow.getChildren()).stream()
        .peek(
            fileRow ->
                new FileRow(
                    folderRow.getFolderName() + File.separator + fileRow.getKey(),
                    fileRow.getValue()))
        .collect(Collectors.toList());
    }
}
