package com.example.jsontocsv.jsonreader;

import com.example.app.model.FolderRows;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultJsonReader implements JsonReader {

    private final JSONParser parser = new JSONParser();

    @Override
    public List<JSONObject> readJsonFolder(String folderPath) {
        Optional<File[]> opt = Optional.ofNullable(new File(folderPath).listFiles());
        return opt.map(this::readJsonFiles)
                .orElse(Collections.emptyList());
    }

  private List<JSONObject> readJsonFiles(File[] files) {
    return Arrays.stream(files)
            .map(file -> new JSONObject(Collections.singletonMap(file.getName(), readJsonFile(file.getPath()))))
            .collect(Collectors.toList());
  }

  @Override
    public JSONObject readJsonFile(String jsonLocation) throws JsonReadException {
        try (FileReader fileReader = new FileReader(jsonLocation)) {
            return  (JSONObject) parser.parse(fileReader);
        } catch (IOException e) {
            throw new JsonReadException(jsonLocation, e);
        } catch (ParseException e) {
            throw new JsonParseException(jsonLocation, e);
        }
    }

    @Override
    public List<FolderRows> readJsonsForLanguages(String folderPath) {
        Optional<File[]> opt = Optional.ofNullable(new File(folderPath).listFiles());
        return opt.map(this::getFolderRows)
                .orElse(Collections.emptyList());
    }

    private List<FolderRows> getFolderRows(File[] files) {
        return Arrays.stream(files)
                        .map(file -> new FolderRows(file.getName(), readJsonFolder(file.getPath())))
                        .collect(Collectors.toList());
    }
}
