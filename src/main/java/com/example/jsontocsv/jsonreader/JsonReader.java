package com.example.jsontocsv.jsonreader;

import com.example.app.model.FolderRows;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * From file to JSONObject
 */
public interface JsonReader {
    /**
     * Accepts path to folder as param
     * @param folderPath
     * @return all json files' content in a single json
     */
    List<JSONObject> readJsonFolder(String folderPath);

    /**
     * Accepts file path as param
     * @param jsonLocation
     * @return file content parsed into Json object
     * @throws Exception in case there is error on filepath or file content
     */
    JSONObject readJsonFile(String jsonLocation) throws Exception;

    /**
     *
     * @param folderPath
     * @return
     */
    List<FolderRows> readJsonsForLanguages(String folderPath);
}
