package com.example.jsontocsv.jsonreader;

class JsonParseException extends RuntimeException{
    JsonParseException(String path, Exception ex) {
        super(String.format("Error while parsing following file:%s", path), ex);
    }
}
