package com.example.jsontocsv.jsonreader;

class JsonReadException extends RuntimeException {
    JsonReadException(String filename, Exception exception) {
        super(String.format("Can not read %s, please try again.", filename), exception);
    }
}
