package com.example.csvtojson.csvreader;

import com.example.app.model.FileRow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class DefaultCvsReaderTest {

    private final CvsReader reader = new DefaultCvsReader();
    private static final String SLASH = File.separator;
    private static final String TEST_RESOURCES = Paths.get(System.getProperty("user.dir"), "src", "test", "resources") + SLASH;

    @Test
    public void readValidFile() {
        String builder = Paths.get(TEST_RESOURCES, "csvFiles", "translation.en.valid.csv").toString();
        List<FileRow> myContent = new ArrayList<>();
        myContent.add(new FileRow("activate:title", "Activation"));
        myContent.add(new FileRow("activate:title.setup2fa", "Setup 2-Factor Authentication"));
        myContent.add(new FileRow("activate:title.complete", "Registration"));
        List<FileRow> fileRows = reader.readFrom(builder);

        Assertions.assertEquals(myContent, fileRows);
    }

    @Test
    void skipsBlankLinesInFile() {
        String builder = Paths.get(TEST_RESOURCES, "csvFiles", "translation.en.withBlankLine.csv").toString();

        List<FileRow> fileRows = reader.readFrom(builder);

        Assertions.assertEquals(3, fileRows.size());
    }

    @Test
    void readFilesFromFolder() {
        List<FileRow> myContent = new ArrayList<>();
        myContent.add(new FileRow("activate:messages:skipped", "<strong>Thank you for registering to NasdaqLEI! You chose to setup Google Authenticator later, so now you can log in to your account using one time code via email, setup Google Authenticator to complete registration and start managing LEI codes.</strong>"));
        myContent.add(new FileRow("activate:messages:complete", "<strong>Thank you for registering to NasdaqLEI! Now you can log in to your account and start managing LEI codes.</strong>"));
        myContent.add(new FileRow("activate:messages:reactivated", "<strong>Your user account activation has been expired.</strong> Please check your email for new confirmation and proceed user activation within 30 minutes."));

        myContent.add(new FileRow("activate:title", "Activation"));
        myContent.add(new FileRow("activate:title.setup2fa", "Setup 2-Factor Authentication"));
        myContent.add(new FileRow("activate:title.complete", "Registration"));

        String builder = TEST_RESOURCES + "csvFiles";

        List<FileRow> listWithCsvRows = reader.readFromFolder(builder);

        Assertions.assertEquals(myContent, listWithCsvRows);
    }

}