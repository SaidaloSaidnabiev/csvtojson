package com.example.csvtojson.csvparser;

import com.example.app.model.FileRow;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DefaultCsvParserTest {
    private final CsvParser parser = new DefaultCsvParser();

    @Test
    public void parseListToJson() {
        List<FileRow> fileRows = new ArrayList<>(Arrays.asList(
                new FileRow("activate:messages:user:initials:name", "Bad Comedian"),
                new FileRow("activate:title", "Activation"),
                new FileRow("activate:title.setup2fa", "Setup 2-Factor Authentication"),
                new FileRow("activate:messages:processing", "Your user account is being activated�"),
                new FileRow("activate:messages:user:title", "Aaasasas")
        ));

        JSONObject messages = new JSONObject();
        messages.put("processing", "Your user account is being activated�");
        JSONObject messageUser = new JSONObject();
        JSONObject mUserInitials = new JSONObject();
        mUserInitials.put("name", "Bad Comedian");
        messageUser.put("initials", mUserInitials);
        messageUser.put("title", "Aaasasas");
        messages.put("user", messageUser);
        JSONObject titleProperty = new JSONObject();
        titleProperty.put("title", "Activation");
        titleProperty.put("title.setup2fa", "Setup 2-Factor Authentication");
        titleProperty.put("messages", messages);
        JSONObject finalObject = new JSONObject();
        finalObject.put("activate", titleProperty);
        JSONObject parsed = parser.parseListToJson(fileRows);
        assertEquals(finalObject, parsed);
    }

    @Test
    public void folderCsvToJsonParser() {
        List<FileRow> fileRowsList = new ArrayList<>();

        fileRowsList.add(new FileRow("translation.en.csv:activate:messages:user:initials:name", "Bad Comedian"));
        fileRowsList.add(new FileRow("translation.en.csv:activate:title", "Activation"));
        fileRowsList.add(new FileRow("translation.en.csv:activate:title.setup2fa", "Setup 2-Factor Authentication"));
        fileRowsList.add(new FileRow("translation.en.csv:activate:messages:processing", "Your user account is being activated�"));
        fileRowsList.add(new FileRow("translation.en.csv:activate:messages:user:title", "Aaasasas"));

        JSONObject messages = new JSONObject();
        messages.put("processing", "Your user account is being activated�");
        JSONObject messageUser = new JSONObject();
        JSONObject mUserInitials = new JSONObject();
        mUserInitials.put("name", "Bad Comedian");
        messageUser.put("initials", mUserInitials);
        messageUser.put("title", "Aaasasas");
        messages.put("user", messageUser);
        JSONObject titleProperty = new JSONObject();
        titleProperty.put("title", "Activation");
        titleProperty.put("title.setup2fa", "Setup 2-Factor Authentication");
        titleProperty.put("messages", messages);
        JSONObject finalObject = new JSONObject();
        finalObject.put("activate", titleProperty);

        JSONObject bigJson = new JSONObject();

        bigJson.put("translation.en.csv", finalObject);

        Assertions.assertEquals(bigJson.get("translation.en.csv"), parser.parseListToJson(fileRowsList).get("translation.en.csv"));
    }
}
