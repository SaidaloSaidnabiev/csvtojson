package com.example.csvtojson.csvwriter;

import com.example.app.model.JSONObjectBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultCsvToJsonWriterTest {
    private final CsvToJsonWriter writer = new DefaultCsvToJsonWriter();
    private static final String SLASH = File.separator;
    private static final String TEST_RESOURCES = Paths.get(System.getProperty("user.dir"), "src", "test", "resources") + SLASH;

    @Test
    public void writeIntoJson() throws IOException, ParseException {
        JSONObject parsedObject = new JSONObjectBuilder().pairWithObjectVal(
                                    "myContent", new JSONObjectBuilder()
                                                            .pairWithStringVal(
                                                                    "newKey",
                                                                    "Test value"
                                                            ).build()
                                                    ).build();
        String builder = Paths.get(TEST_RESOURCES, "jsonFiles", "translation.en.json").toString();
        File myFile = new File(builder);
        writer.writeToJsonFile(myFile, parsedObject);
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(builder));
        JSONObject fileContent = (JSONObject) obj;
        Assertions.assertEquals(parsedObject, fileContent);
    }

    @Test
    public void writeIntoLanguageJsons_success_listOfGeneratedFiles() {
        JSONObject translations = new JSONObjectBuilder().pairWithObjectVal(
                Paths.get("en", "activate.json").toString(),
                new JSONObjectBuilder().pairWithStringVal("activate", "Saidalo").build()
        ).pairWithObjectVal(
                Paths.get("lt","addressTypeEnum.json").toString(),
                new JSONObjectBuilder().pairWithStringVal("leiApp", "for testing").build()
        ).pairWithObjectVal(
                Paths.get("lv", "test.json").toString(),
                new JSONObjectBuilder().pairWithObjectVal(
                        "form",
                        new JSONObjectBuilder().pairWithStringVal(
                                "skip", "Skip"
                        ).pairWithStringVal(
                                "complete", "Complete registration"
                        ).pairWithStringVal(
                                "login", "Go to Login"
                        ).build()
                ).build()
        ).build();

        List<String> generatedFiles = writer.writeToJsonFiles(Paths.get(TEST_RESOURCES, "jsonFiles") + SLASH,
                                                                translations).stream().map(File::getName)
                                                             .collect(Collectors.toList());
        List<String> filesList = Arrays.asList("addressTypeEnum.json", "activate.json", "test.json");
        Assert.assertEquals(generatedFiles, filesList);
    }
}