package com.example.jsontocsv.jsonwriter;

import com.example.app.model.FileRow;
import com.example.jsontocsv.jsontocsv.DefaultCsvWriter;
import com.example.jsontocsv.jsontocsv.ToCsvWriter;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DefaultJsonWriterTest {
    private static final ToCsvWriter writer = new DefaultCsvWriter();
    private static final String SLASH = File.separator;
    private static final String TEST_RESOURCES = String.join(SLASH, System.getProperty("user.dir"), "src",
                                                    "test", "resources");

    @Test
    public void writeIntoJson() {
        List<FileRow> fileRows = new ArrayList<>(Arrays.asList(
                new FileRow("activate:messages:user:initials:name", "Bad Comedian"),
                new FileRow("activate:title", "Activation"),
                new FileRow("activate:title.setup2fa", "Setup 2-Factor Authentication"),
                new FileRow("activate:messages:processing", "Your user account is being activated�"),
                new FileRow("activate:messages:user:title", "Aaasasas")
        ));

        String builder = String.join(SLASH, TEST_RESOURCES, "csvFiles", "activate.csv");
        File myFile = writer.writeToCsv(builder, fileRows);
        assertTrue(myFile.exists());
    }
}
