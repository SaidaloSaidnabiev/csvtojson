package com.example.jsontocsv.jsonreader;

import com.example.app.model.FolderRows;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DefaultJsonReaderTest {
    private JsonReader jsonReader = new DefaultJsonReader();

    @Test
    public void readFromFolder_success_listOfJsonFileContent() {
        String folderPath = System.getProperty("user.dir") + "/src/test/resources/jsonFiles";
        List<JSONObject> myJsons = jsonReader.readJsonFolder(folderPath);
        //--------First file----------------
        JSONObject f1 = new JSONObject();
        JSONObject form = new JSONObject();
        form.put("skip", "Skip");
        form.put("complete", "Complete registration");
        form.put("login", "Go to Login");
        f1.put("form", form);
        //--------Second file----------------
        JSONObject f2 = new JSONObject();
        f2.put("leiApp", "for testing");
        //--------Third file----------------
        JSONObject f3 = new JSONObject();
        f3.put("activate", "Saidalo");

        List<JSONObject> folderContent = new ArrayList<>();
        folderContent.add(f2);
        folderContent.add(f3);
        folderContent.add(f1);

        Assert.assertEquals(folderContent, myJsons);
    }

    @Test
    public void readFromFile_success_jsonFileContent() throws Exception {
        String filePath = System.getProperty("user.dir") + "/src/test/resources/jsonFiles/test.json";
        JSONObject testJson = new JSONObject();
        JSONObject form = new JSONObject();

        form.put("skip", "Skip");
        form.put("complete", "Complete registration");
        form.put("login", "Go to Login");

        testJson.put("form", form);

        Assert.assertEquals(testJson, jsonReader.readJsonFile(filePath));
    }

    @Test
    public void readFolder_fileNotExist_jsonReadExceptionThrown() {
        String folderPath = System.getProperty("user.dir") + "/src/test/resources/jsonFile1";
        Assert.assertEquals(jsonReader.readJsonFolder(folderPath), Collections.emptyList());
    }

    @Test
    public void readJsonFile_fileNotExist_jsonReadExceptionThrown() {
        String filePath = System.getProperty("user.dir") + "/src/test/resources/jsonFile/aaa.json";
        JsonReadException myException = Assert.assertThrows(JsonReadException.class, () -> {
            jsonReader.readJsonFile(filePath);
        });
        String message = myException.getMessage();
        Assert.assertEquals(String.format("Can not read %s, please try again.", filePath), message);
    }

    @Test
    public void parsing_jsonParsingFail_jSonParseExceptionThrown() {
        String filePath = System.getProperty("user.dir") + "/src/test/resources/test.txt";
        JsonParseException myException = Assert.assertThrows(JsonParseException.class, () -> {
            jsonReader.readJsonFile(filePath);
        });

        Assert.assertEquals(String.format("Error while parsing following file:%s", filePath), myException.getMessage());
    }

    @Test
    public void readJsonFiles_success_listOfJsonFiles() {
        String languagePath = System.getProperty("user.dir") + "/src/test/resources/json/";
        JSONObject active = new JSONObject();
        active.put("activate", "Saidalo");
        JSONObject activeFile = new JSONObject();
        activeFile.put("activate.json", active);

        JSONObject addressType = new JSONObject();
        addressType.put("leiApp", "for testing");
        JSONObject addressTypeFile = new JSONObject();
        addressTypeFile.put("addressTypeEnum.json", addressType);

        JSONObject testJson = new JSONObject();
        JSONObject form = new JSONObject();
        form.put("skip", "Skip");
        form.put("complete", "Complete registration");
        form.put("login", "Go to Login");
        testJson.put("form", form);
        JSONObject testJsonFile = new JSONObject();
        testJsonFile.put("test.json", testJson);

        List<FolderRows> compareWith = Arrays.asList(
                new FolderRows("lv", Arrays.asList(testJsonFile)),
                new FolderRows("en", Arrays.asList(activeFile)),
                new FolderRows("lt", Arrays.asList(addressTypeFile))
        );
        List<FolderRows> folderRows = jsonReader.readJsonsForLanguages(languagePath);
        Assert.assertEquals(compareWith, folderRows);
    }
}
