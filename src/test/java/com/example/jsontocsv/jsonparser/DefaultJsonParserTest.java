package com.example.jsontocsv.jsonparser;

import com.example.app.model.JSONObjectBuilder;
import com.example.app.model.FileRow;
import com.example.app.model.FolderRows;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class DefaultJsonParserTest {
    private static final JsonParser parser = new DefaultJsonParser();
    @Test
    public void parseJsonToWritable() {
        List<FileRow> fileRows = new ArrayList<>(Arrays.asList(
                new FileRow("activate:title.setup2fa", "Setup 2-Factor Authentication"),
                new FileRow("activate:messages:processing", "Your user account is being activated�"),
                new FileRow("activate:messages:user:initials:name", "Bad Comedian"),
                new FileRow("activate:messages:user:title", "Aaasasas"),
                new FileRow("activate:title", "Activation")
        ));

        JSONObject messages = new JSONObject();
        messages.put("processing", "Your user account is being activated�");
        JSONObject messageUser = new JSONObject();
        JSONObject mUserInitials = new JSONObject();
        mUserInitials.put("name", "Bad Comedian");
        messageUser.put("initials", mUserInitials);
        messageUser.put("title", "Aaasasas");
        messages.put("user", messageUser);
        JSONObject titleProperty = new JSONObject();
        titleProperty.put("title", "Activation");
        titleProperty.put("title.setup2fa", "Setup 2-Factor Authentication");
        titleProperty.put("messages", messages);
        JSONObject finalObject = new JSONObject();
        finalObject.put("activate", titleProperty);

        List<FileRow> writable = parser.parseIntoList(finalObject);

        assertEquals(writable, fileRows);
    }

    @Test
    public void parseMany_success_listOfJsonContent() {
        List<FileRow> fileRows = List.of(
                new FileRow("activate", "Saidalo"),
                new FileRow("leiApp", "for testing"),
                new FileRow("form:skip", "Skip"),
                new FileRow("form:complete", "Complete registration"),
                new FileRow("form:login", "Go to Login")
        );

        JSONObject f1 = new JSONObject();
        JSONObject form = new JSONObject();
        form.put("skip", "Skip");
        form.put("complete", "Complete registration");
        form.put("login", "Go to Login");
        f1.put("form", form);
        //--------Second file----------------
        JSONObject f2 = new JSONObject();
        f2.put("leiApp", "for testing");
        //--------Third file----------------
        JSONObject f3 = new JSONObject();
        f3.put("activate", "Saidalo");

         final List<JSONObject> folderContent = List.of(f3, f2, f1);

        List<FileRow> entireFolder = parser.parseFolderContentIntoList(folderContent);
        assertEquals(fileRows, entireFolder);
    }

    @Test
    public void parseIntoBigList_success_listOfFileRows() {
        JSONObject activeFile = (new JSONObjectBuilder()).pairWithStringVal("activate", "Saidalo").build();

        JSONObject addressTypeFile = (new JSONObjectBuilder()).pairWithStringVal("leiApp", "for testing").build();

        JSONObject testJsonFile = (new JSONObjectBuilder()).pairWithObjectVal(
                                                    "form",
                                                        (new JSONObjectBuilder())
                                                                .pairWithStringVal("skip", "Skip")
                                                                .pairWithStringVal("complete", "Complete registration")
                                                                .pairWithStringVal("login", "Go to Login")
                                                                .build()
                                                        ).build();

        List<FolderRows> compareWith = Arrays.asList(
                new FolderRows("lv", Arrays.asList(testJsonFile)),
                new FolderRows("en", Arrays.asList(activeFile)),
                new FolderRows("lt", Arrays.asList(addressTypeFile))
        );

        List<FileRow> fileRows = List.of(
                new FileRow("form:skip", "Skip"),
                new FileRow("form:complete", "Complete registration"),
                new FileRow("form:login", "Go to Login"),
                new FileRow("activate", "Saidalo"),
                new FileRow("leiApp", "for testing")
        );

        List<FileRow> parsedIntoList = parser.parseIntoBigList(compareWith);

        assertEquals(fileRows, parsedIntoList);
    }
}
